<?php

/**
 * @file
 * Template file for Image srcset formatter.
 *
 * Available variables:
 * - $src_url: The URL to the image.
 * - $srcset_url: The srcset of the image.
 * - $sizes: The screen sizes set to the image.
 * - $img_alt: The alt of the image.
 * - $img_title: The title of the image.
 */
?>

<img src="<?php print $src_url?>" <?php (!empty($srcset_url)) ? print "srcset='" . $srcset_url . "'" : FALSE;?> <?php (!empty($sizes)) ? print "sizes='" . $sizes . "'" : FALSE;?> <?php (!empty($img_title)) ? print "title='" . $img_title . "'" : FALSE; ?> <?php (!empty($img_alt)) ? print "alt='" . $img_alt . "'" : FALSE;?> >
