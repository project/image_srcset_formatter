CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Maintainers


INTRODUCTION
------------

This module allows you to define image style on multiple screen size. 
The "srcset" defines a set of images that will allow the browser to 
choose between.

It will still use the "src" attribute to identify a “default” image source 
to be used in browsers that don't support "srcset".


INSTALLATION
-------------

1.) Install and enable this module.
2.) Create an image style that you want to be applied on every screen size.
3.) Go to "Manage display" on the image that you want to use srcset.
4.) Set up the formatter to "Image srscet".
5.) Change the settings and add an image style that will be 
used per screen size.


MAINTAINERS
-----------

Carlo Miguel Agno (carlagno) - https://www.drupal.org/user/3317429
